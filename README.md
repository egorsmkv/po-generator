# PO files generator

A tool to generate PO files for the Veloren project.

## Running

Just run the generator.py file:

```
python generate.py
```
