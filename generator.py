from strings import ALL

FILE_WRITE = 'data/en.po'
HEADER = '''# Veloren Translator Team <box@example.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: Veloren\\n"
"Report-Msgid-Bugs-To: \\n"
"POT-Creation-Date: 2020-01-01 00:01+0000\\n"
"PO-Revision-Date: 2020-01-01 00:01+0000\\n"
"Last-Translator: Veloren\\n"
"Language-Team: English\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"Language: en\\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\\n"

\n'''


def run():
    with open(FILE_WRITE, 'a') as file:
        # Add header
        file.write(HEADER)

        # Add strings
        for k, v in ALL.items():
            lines = v.splitlines()

            if len(lines) > 1:
                data = []
                for line in lines:
                    data.append('"{}\\n"'.format(line))
                file.write('msgid ' + '\n'.join(data))
                file.write('\nmsgstr ""')
                file.write('\n\n')
            else:
                file.write('msgid "{}"\n'.format(v))
                file.write('msgstr ""')
                file.write('\n\n')


if __name__ == '__main__':
    run()
